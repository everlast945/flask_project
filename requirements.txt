# core
Flask==1.1.2
Flask-DebugToolbar==0.11.0
Flask-Migrate==2.5.3
Flask-RESTful==0.3.8
Flask-SQLAlchemy==2.4.4
Flask-Script==2.0.5
Werkzeug==1.0.1
click==7.1.2
blinker==1.4
itsdangerous==1.1.0
pytz==2020.1
six==1.15.0

# database
SQLAlchemy==1.3.18
alembic==1.4.2
psycopg2==2.8.5

# templates
Jinja2==2.11.2
MarkupSafe==1.1.1
Mako==1.1.3

# sugar
python-dateutil==2.8.1
python-editor==1.0.4
python-slugify==4.0.1
setuptools==49.2.1
text-unidecode==1.3
wheel==0.34.2
python-dotenv==0.14.0

# от куда то принянулась. Работа с датами
aniso8601==8.0.0






