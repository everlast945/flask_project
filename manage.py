#!/usr/bin/env python
import os

from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from app import create_app
from app.database import db
from dotenv import load_dotenv

# Загружаем переменные в виртуальное окружение
load_dotenv()

# Создание приложения
app = create_app()

# Подлючается менеджер для запуска команд
manager = Manager(app)

# Добавляются команды для работы с миграциями
migrate = Migrate(app, db)
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
