import os
from flask import Flask
from flask_restful import Api

from .database import db
from .entity.routes import initialize_routes


def create_app():
    app = Flask(__name__)
    app.config.from_object(os.environ['APP_SETTINGS'])

    api = Api(app)

    db.init_app(app)
    with app.test_request_context():
        db.create_all()

    # Подключение модулей
    import app.entity.controllers as entity
    app.register_blueprint(entity.module)
    initialize_routes(api)

    return app