from app.database import db
from app.utils.models import BaseModel, DateModelMixin


class Comment(BaseModel, DateModelMixin):
    __tablename__ = 'comment'

    text = db.Column(db.String(50))
    entity_id = db.Column(db.Integer, db.ForeignKey('entity.id'))
