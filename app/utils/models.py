from flask_sqlalchemy import BaseQuery

from app.database import db


class BaseModel(db.Model):
    __abstract__ = True
    __tablename__ = ''

    id = db.Column(db.Integer, primary_key=True)
    
    query = BaseQuery


class DateModelMixin(db.Model):
    __abstract__ = True

    created_on = db.Column(db.DateTime, default=db.func.now)
    updated_on = db.Column(db.DateTime, default=db.func.now, onupdate=db.func.now)
