from .controllers import EntityListView, EntityDetailView

def initialize_routes(api):
    api.add_resource(EntityListView, '/')
    api.add_resource(EntityDetailView, '/<int:pk>')
