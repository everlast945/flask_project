from flask import Blueprint
from flask_restful import Resource
from .models import Entity
from app.comment.models import Comment

module = Blueprint('entity', __name__)


class EntityListView(Resource):
    def get(self):
        return {'hello': 'world'}


class EntityCreateView(Resource):
    def post(self):
        return {'hello': 'world'}


class EntityDetailView(Resource):
    def get(self, pk):
        Entity.query.get_or_404(pk)
        return {'hello': 'world'}


class EntityEditView(Resource):
    def put(self):
        return {'hello': 'world'}
