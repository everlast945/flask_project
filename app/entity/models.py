from app.database import db
from app.utils.models import BaseModel, DateModelMixin


class Entity(BaseModel, DateModelMixin):
    __tablename__ = 'entity'

    name = db.Column(db.String(1000), nullable=False, unique=True)
    short_text = db.Column(db.String(50))

    comments = db.relationship('Comment', backref='entity')

    def __str__(self):
        return self.name


class Report(BaseModel, DateModelMixin):
    __tablename__ = 'report'
